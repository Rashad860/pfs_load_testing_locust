import groovy.json.*
evaluate(new File("./kubernetes.groovy"))
evaluate(new File("./helm.groovy"))

def jsonSlurper = new JsonSlurper()
def reader = new BufferedReader(new InputStreamReader(new FileInputStream("config.json"),"UTF-8"))

configFile = jsonSlurper.parse(reader)

configFile.locust_installations.each {
	def user = configFile.user
	def release = configFile.user + '-' + it.release + 'locust'
	helmDelete(['--purge', release])
	deleteNamespace([it.namespace])
}

configFile.installations.each {
	def user = configFile.user
	def release = configFile.user + '-' + it.release
	helmDelete(['--purge', release])
	deleteNamespace([it.namespace])
}