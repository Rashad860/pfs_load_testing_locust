# Load Testing PFS with Locust

## Prerequisites:
* Clone this repo
* Installed groovy
* In the config.json, update your docker creds.
* Using a linux-like terminal, cd into this project's directory and run 'groovy load-setup.groovy'
* To start the swarm run 'groovy swarm.groovy'

## Configuration (Config.json)
* docker_username: Username for image repo. Currently quay.
* docker_password: Password for image repo.
* docker_email: Email registered to account for image repo.
* docker_server: Endpoint to image repo server.
* chart: Fullpath + tag to image under test.
* locust_chart: Fullpath + tag to locust image.
* user: Serves as a prefix for certain resources.
* base_host: Base url for pfs environment.
* installations: Configuration object for a single pfs instance.
* installations.length: Starting index for chart under test. It will increment per installation.
* installations.namespace: Namespace to put the installation. The current length will serve as a suffix for the namespace.
* installations.values_file: Location of values.yaml file for pfs installation.
* locust_installations: Configuration for locust installation.
* locust_installations.length: Starting index for chart loucust chart. It will increment per installation.
* locust_installations.port_start: This is the starting port for locust. It will increment per installation.
* locust_installations.namespace: Namespace to put the installation. The current length will serve as a suffix for the namespace.
* locust_installations.hatch_rate: Users spawned/second
* locust_installations.locust_count: Numbers of users to simulate
* locust_installations.values_file: Location of values.yaml file for locust chart.
