
sh = { args ->
	if(args.getClass() != "".getClass()) {
		commandString = args.toList().join(' ')
	}
	proc = commandString.execute()
	proc.waitFor() 
	def pec = proc.exitValue()
	def std_err = proc.err.text
	def std_out = proc.in.text
	println "Command: ${commandString}"
	println "Process exit code: ${pec}"
	println "Std Err: ${std_err}"
	println "Std Out: ${std_out}"
	return ['process_exit_code':pec, 'str_err':std_err, 'std_out':std_out]
}