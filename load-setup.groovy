import groovy.json.*
evaluate(new File("./kubernetes.groovy"))
evaluate(new File("./helm.groovy"))

def jsonSlurper = new JsonSlurper()
def reader = new BufferedReader(new InputStreamReader(new FileInputStream("config.json"),"UTF-8"))

configFile = jsonSlurper.parse(reader)

int count = 0
int installation_limit = configFile.installations.length
def user = configFile.user
def chart = configFile.chart
def docker_username = configFile.docker_username
def docker_password = configFile.docker_password
def docker_email = configFile.docker_email
def docker_server = configFile.docker_server

config = configFile.installations

while (count < installation_limit){
	def namespace = config.namespace + count
	def values_file = config.values_file
	def prefix = user + '-' + count + '-'
	def release = prefix + 'pfs'
	pfs_host = prefix + configFile.base_host

	createNamespace([namespace])

	createSecret(['docker-registry', 'core-registry', 
				  '--docker-server=' + docker_server, 
				  '--docker-username=' + docker_username,  
				  '--docker-password=' + docker_password, 
				  '--docker-email=' + docker_email, 
				  '--namespace', namespace])

	helmRegistryInstall([chart, 
						 '--name', release,
						 '--namespace', namespace,
						 '--values', values_file,
						 '--set', 'pfs-webapp-chart.ingress.host=' + pfs_host
						 ])
	count++
}

int locust_count = 0
String max = configFile.locust_installations.length
int limit = max as Integer
it = configFile.locust_installations
Integer port = configFile.locust_installations.port_start

while (locust_count < limit){
	def base_url = user + '-' + locust_count + '-'
	def locust_release = base_url + 'locust'
	host = 'http://' + base_url + configFile.base_host
	namespace = it.namespace + locust_count

	createNamespace([namespace])

	createSecret(['docker-registry', 'core-registry', 
			  '--docker-server=' + docker_server, 
			  '--docker-username=' + docker_username,  
			  '--docker-password=' + docker_password, 
			  '--docker-email=' + docker_email, 
			  '--namespace', namespace])

	helmRegistryInstall([configFile.locust_chart, 
					 '--name', locust_release,
					 '--namespace', namespace,
					 '--values', 'values.yaml',
					 '--set', 'master.config.target-host=' + host,
					 '--set', 'service.nodePort=' + port])

	locust_count++
	port++
}