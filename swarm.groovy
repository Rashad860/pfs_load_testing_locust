import groovy.json.*
evaluate(new File("./kubernetes.groovy"))

def jsonSlurper = new JsonSlurper()
def reader = new BufferedReader(new InputStreamReader(new FileInputStream("config.json"),"UTF-8"))

configFile = jsonSlurper.parse(reader)

configFile.locust_installations.each {
	kubectlRun(['-n', it.namespace, 
	'strzal', '--image=djbingham/curl', 
	'--restart=OnFailure',
	 '-i', '--tty', '--rm', '--command', '--', 
	 'curl', '-X', 'POST', 
	 '-F', 'host=' + it.pfs_host,
	 '-F', 'locust_count=' + it.locust_count, 
	 '-F', 'hatch_rate=' + it.hatch_rate,
	  it.locust_master_host])
}
