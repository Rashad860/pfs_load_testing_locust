evaluate(new File("./shell.groovy"))

createNamespace = { args ->
	commandParameters = args.toList()
	commandParameters.add(0, "kubectl")
	commandParameters.add(1, 'create')
	commandParameters.add(2, 'ns')
	commandString = commandParameters.join(' ')
	return sh(commandString)
}

deleteNamespace = { args ->
	commandParameters = args.toList()
	commandParameters.add(0, "kubectl")
	commandParameters.add(1, 'delete')
	commandParameters.add(2, 'ns')
	commandString = commandParameters.join(' ')
	return sh(commandString)
}

createSecret = { args ->
	commandParameters = args.toList()
	commandParameters.add(0, "kubectl")
	commandParameters.add(1, 'create')
	commandParameters.add(2, 'secret')
	commandString = commandParameters.join(' ')
	return sh(commandString)
}

getNamespaces = { ->
	return sh('kubectl get namespaces')
}

kubectlRun = { args ->
	commandParameters = args.toList()
	commandParameters.add(0, "kubectl")
	commandParameters.add(1, 'run')
	commandString = commandParameters.join(' ')
	return sh(commandString)
}