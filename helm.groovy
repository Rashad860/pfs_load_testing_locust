evaluate(new File("./shell.groovy"))

helmRegistryInstall = { args ->
	commandParameters = args.toList()
	commandParameters.add(0, "helm")
	commandParameters.add(1, 'registry')
	commandParameters.add(2, 'install')
	commandString = commandParameters.join(' ')
	return sh(commandString)
}

helmList = { args ->
	commandParameters = args.toList()
	commandParameters.add(0, "helm")
	commandParameters.add(1, 'list')
	commandString = commandParameters.join(' ')
	return sh(commandString)
}

helmDelete = { args ->
	commandParameters = args.toList()
	commandParameters.add(0, "helm")
	commandParameters.add(1, 'delete')
	commandString = commandParameters.join(' ')
	return sh(commandString)
}